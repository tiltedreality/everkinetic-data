# Sit Ups

> This is an exercise for abdominal muscle strengthening.

``` 
id: 0304 
type: isolation 
primary: rectus abdominis 
secondary: obliques,hip flexors 
equipment: exercise mat 
``` 


## Steps


 - Lie down on your back on an exercise mat, bending your knees and placing your feet flat on the ground.
 - Place your hands behind your head or crossed on your chest.
 - Engage your core muscles and lift your upper body towards your knees, keeping your lower back pressed against the mat.
 - Pause for a moment at the top, then slowly lower your upper body back down to the starting position.

## Tips


 - Do not pull on your neck with your hands while performing this exercise.

## Images


