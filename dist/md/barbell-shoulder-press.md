# Barbell Shoulder Press

> This is an exercise for shoulder, chest and triceps strengthening.

``` 
id: 0303 
type: isolation 
primary: deltoid 
secondary: triceps brachii,pectoralis major 
equipment: barbell 
``` 


## Steps


 - Stand with feet about shoulder width apart and grasp the barbell with a grip 3 to 4 inches wider than your shoulders.
 - Lift the bar off the rack and lower it to just at the height of your shoulders.
 - While maintaining  good posture, straighten your arms and raise the bar up above your head.
 - Pause for a moment and then in a controlled movement lower the bar to the starting position.

## Tips


 - Do not perform this exercise if you have shoulder, elbow or wrist pain.

## Images


